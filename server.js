const express = require('express');
const app = express();
app.set('view engine', 'pug');
app.set('views','./views');
app.get('/', (request, response) => {
  response.send("Hello");
});
var todos = [
  {
    id: 1,
    content: 'Đi chợ',
  },
  {
    id: 2,
    content: 'Nấu cơm',
  },
  {
    id: 3,
    content: 'Rửa bát',
  },
  {
    id: 4,
    content: 'Học code tại CodersX',
  },
];
app.get('/todos', (req, res) => {
  res.render('index',{todos: todos});
});
// listen for requests :)
app.listen(3000, () => {
  console.log('Server listening on port ' + process.env.PORT);
});
